package tools.database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import windhelm.Server;
import windhelm.util.Misc;

public class DumpData {

    private static final String DUMP_DIR = "./Data/backups/Database Dumps/";

    public static boolean dumpAll() {
        String[] all = {"achievements","bank_0","bank_1","bank_2","bank_3","bank_4","appearance","equipment","items","players","points","skills"};
        int count = 0;
        for(String string : all)
            if(dumpData(string))
                count++;
        return count == all.length;
    }

    /**
     * Loads 1d String array data stored in the player database
     */
    public static boolean dumpData(String table) {
        try {
            System.out.println("Attempting to dump the " + Misc.optimizeText(table) + " table...");
            if(Pool.createConnection(Pool.DATABASE_LOCAL)) {
                ArrayList<String> a = new ArrayList<String>();
                String query = "SELECT * FROM `" + table + "`";
                ResultSet rs = Pool.query(query, Pool.savePoolID, Pool.saveConID);
                switch(table) {

                case "achievements":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `achieveData`) VALUES");
                    while(rs.next())
                        a.add("(" + rs.getString("playerID") + 
                                ", '" + rs.getString("achieveData") + "'),");
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "bank_0":
                case "bank_1":
                case "bank_2":
                case "bank_3":
                case "bank_4":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `itemID`, `itemSlot`, `itemAmount`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("itemID") + 
                                ", " + rs.getString("itemSlot") + 
                                ", " + rs.getString("itemAmount") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "appearance":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `lookIds`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("lookIds") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "equipment":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `equipIds`, `equipAmts`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("equipIds") + 
                                ", " + rs.getString("equipAmts") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "items":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `itemIds`, `itemAmts`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("itemIds") + 
                                ", " + rs.getString("itemAmts") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "players":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `dateCreated`, `playerName`, `playerPass`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("dateCreated") + 
                                ", " + rs.getString("playerName") + 
                                ", " + rs.getString("playerPass") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "points":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `prestigePoints`, `prestigeTokens`, `dungPoints`, "
                            + "`pestPoints`, `windhelmPoints`, `donatorPoints`, `pkPoints`, "
                            + "`slayerPoints`, `magePoints`, `auctionPoints`, "
                            + "`zombiePoints`, `staffPoints`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("prestigePoints") + 
                                ", " + rs.getString("prestigeTokens") +  
                                ", " + rs.getString("dungPoints") +  
                                ", " + rs.getString("pestPoints") +  
                                ", " + rs.getString("windhelmPoints") +  
                                ", " + rs.getString("donatorPoints") +  
                                ", " + rs.getString("pkPoints") +  
                                ", " + rs.getString("slayerPoints") +  
                                ", " + rs.getString("magePoints") +  
                                ", " + rs.getString("auctionPoints") +  
                                ", " + rs.getString("zombiePoints") +  
                                ", " + rs.getString("staffPoints") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                case "skills":
                    a.add("INSERT INTO `" + table + "` (`playerID`, `skillID`, `skillLevel`, `skillXP`) VALUES");
                    while(rs.next()) {
                        a.add("(" + rs.getString("playerID") + 
                                ", " + rs.getString("skillID") + 
                                ", " + rs.getString("skillLevel") + 
                                ", " + rs.getString("skillXP") + "),");
                    }
                    return writeDump(a, "" + Misc.optimizeText(table) + "_" + Server.calendar.getShortVersion(false) + ".txt");

                }
                Pool.doneUsing(Pool.savePoolID, Pool.saveConID);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean writeDump(ArrayList<String> data, String file) {
        try {
            File dir = new File(DUMP_DIR + Server.calendar.getShortVersion(false) + "/");
            if(!dir.exists())
                dir.mkdirs();
            else {
                dir.delete();
                dir.mkdirs();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(DUMP_DIR + Server.calendar.getShortVersion(false) + "/" + file));
            try {
                for(String string : data) {
                    if(string != null) {
                        out.write(string);
                        out.newLine();
                    }
                }
            } finally {
                out.close();
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}