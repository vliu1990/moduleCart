package maintenance;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlWriter;
import org.junit.Test;

public class ExportTest {
	@Test
	public void test() throws Exception {
		String featuresName = "plugins";
		String tableName = "product";
		IDatabaseConnection connection = Utils.getConnection();
		QueryDataSet backupDataSet = new QueryDataSet(connection);
		backupDataSet.addTable(tableName);
		// File file=File.createTempFile(tableName,".xml",new
		// File("target\\"));//备份文件
		// File file=new File(".xml");//备份文件
		File dir = new File("src/test/resources/META-INF/data/" + featuresName + "/data_xml");
		if (!dir.exists())
			dir.mkdirs();
		File xmlFile = new File(dir, tableName + ".xml");
		// FlatXmlDataSet.write(backupDataSet,new FileOutputStream(xmlFile));
		// SqlLoaderControlDataSet set=new SqlLoaderControlDataSet(xmlFile,
		// xmlFile);
		// set.
		Writer writer = new FileWriter(xmlFile);
		// SqlLoaderControlDataSet set=new SqlLoaderControlDataSet(writer);
		// XmlDataSetWriter w = new XmlDataSetWriter(writer);
		FlatXmlWriter w = new FlatXmlWriter(writer);
		w.write(backupDataSet);
		writer.flush();
		writer.close();
		// SQLHelper.
		// XmlDataSetWriter w;
	}
}
